Gunicorn
=========
Installs Gunicorn WSGI HTTP Server.  

Requirements:
* Python3 >= 3.5

Include role
------------
```yaml
- name: gunicorn  
  src: https://gitlab.com/ansible_roles_v/gunicorn/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: hosts
  gather_facts: true
  become: true
  roles:
    - gunicorn
```